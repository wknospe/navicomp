
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class Sprite implements Drawable, Comparable<Sprite>{
    String file;
    Image img;
    BufferedImage original;
    Rectangle hitbox;
    boolean[][] hitmask;
    Point2D[] points;
    int pCount;
    int x;
    int y;
    float rx;
    float ry;
    float xVel = 0;
    float yVel = 0;
    int layer = 0;
    int width;
    int height;
    float scaling;
    float centerX;
    float centerY;
    AffineTransform transform = new AffineTransform();
    GraphicsWindow parent;
    boolean controllable = false;
    boolean deleteNext = false;
    boolean solid = true;
    boolean clickable = false;
    double theta = 0;
    double thetaVel = 0;
    static Random rgen = new Random();
    List<Sprite> toSpawn = new ArrayList<Sprite>();
    public Sprite(String imgfile, int x, int y, GraphicsWindow parent, float scaling) {
        try {
            File pathToFile = new File(imgfile);
            original = ImageIO.read(pathToFile);
            img = original.getScaledInstance(Math.round(scaling*original.getWidth()), -1, Image.SCALE_FAST);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        file = imgfile;
        this.x = x - (img.getWidth(null)/2);
        this.y = y - (img.getHeight(null)/2);
        this.centerX = img.getWidth(null)/2;
        this.centerY = img.getHeight(null)/2;
        rx = (float) this.x;
        ry = (float) this.y;
        this.parent = parent;
        this.scaling = scaling;
        initHitbox();
    }
    public Sprite copy(int cx, int cy) {
        return new Sprite(file, cx, cy, parent, scaling);
    }
    public Sprite(BufferedImage original, int x, int y, GraphicsWindow parent, float scaling) {
        file = "UNKNOWN";
        this.original = original;
        img = original.getScaledInstance(Math.round(scaling*original.getWidth()), -1, Image.SCALE_FAST);
        this.centerX = img.getWidth(null)/2;
        this.centerY = img.getHeight(null)/2;
        this.x = x - (img.getWidth(null)/2);
        this.y = y - (img.getHeight(null)/2);
        rx = (float) this.x;
        ry = (float) this.y;
        this.parent = parent;
        this.scaling = scaling;
        initHitbox();
    }
    public void setCenter(int cx, int cy) {
        centerX = cx;
        centerY = cy;
    }
    public void initHitbox() {
        assert original != null;
        this.width = img.getWidth(null);
        this.height = img.getHeight(null);
        this.hitbox = new Rectangle(x,y,width, height);
        this.hitmask = new boolean[width][height];
        points = new Point[width*height];
        //int bufWidth = original.getWidth();
        //int bufHeight = original.getHeight();
        pCount = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (new Color(original.getRGB((int) Math.floor(i/scaling),(int) Math.floor(j/scaling)), true).getAlpha() != 0) {
                    hitmask[i][j] = true;
                    points[pCount] = new Point(i,j);
                    pCount++;
                } else {
                    hitmask[i][j] = false;
                }
            }
        }
    }
    public boolean checkCollision(Sprite other) {
        if (hitbox.intersects(other.hitbox)) {
            return checkHitmask(other);
        } else {
            return false;
        }
    }
    public boolean checkHitmaskAt(int xrl, int yrl) {
        //converts to from center
        int xfc = xrl - getCenterX();
        int yfc = yrl - getCenterY();
        //checks from the center of the sprite.
        int myx = (int) (xfc*Math.cos(theta) + yfc*Math.sin(theta));
        int myy = (int) (yfc*Math.cos(theta) - xfc*Math.sin(theta));
        try {
            return hitmask[hitmask.length/2 + myx][hitmask[0].length/2 + myy];
        } catch (Exception e) {
            return false;
        }
    }
    public int getXBound() {
        return (int) Math.abs(Math.abs(width*Math.cos(theta)/2) + Math.abs(height*Math.sin(theta)/2));
    }
    public int getYBound() {
        return (int) Math.abs(Math.abs(height*Math.cos(theta)/2) + Math.abs(width*Math.sin(theta)/2));
    }
    public boolean checkHitmask(Sprite other) {
        //System.out.println("Checking at " + x + "," +  y);
        int preX = Math.max(-getXBound() + getCenterX(), -other.getXBound() + other.getCenterX());
        int preY = Math.max(-getYBound() + getCenterY(), -other.getYBound() + other.getCenterY());
        int postX = Math.min(getXBound() + getCenterX(), other.getXBound() + other.getCenterX());
        int postY = Math.min(getYBound() + getCenterY(), other.getYBound() + other.getCenterY());
        for (int i = preX; i < postX; i++) {
            for (int j = preY; j < postY; j++) {
                if (checkHitmaskAt(i,j) && other.checkHitmaskAt(i, j)) {
                    return true;
                }
            }
        }
        return false;
        /*
        int deltax = other.x - x;
        int x0 = Math.max(0, deltax);
        int xf = Math.min(hitmask.length, other.hitmask.length + deltax);
        int deltay = other.y - y;
        int y0 = Math.max(0, deltay);
        int yf = Math.min(hitmask[0].length, other.hitmask[0].length + deltay);
        for (int i = x0; i < xf; i++) {
            for (int j = y0; j < yf; j++) {
                if (hitmask[i][j] && other.hitmask[i - deltax][j - deltay]) {
                    return true;
                }
            }
        }
        return false;*/
    }
    public void handleCollision(Sprite other) {
        
    }
    public int getCenterX() {
        return x + (img.getWidth(null)/2);
    }
    public int getCenterY() {
        return y + (img.getHeight(null)/2);
    }
    public void rotate(double deltaTheta) {
        theta = theta + deltaTheta;
        transform.setToRotation(theta,centerX, centerY);
    }
    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        AffineTransform shift = new AffineTransform();
        shift.translate(x, y);
        shift.concatenate(transform);
        g2d.drawImage(img, shift , null);
        //g2d.setColor(Color.RED);
        //g2d.draw(hitbox);
    }
    public void accelerate(double deltaV) {
        xVel = (float) (xVel + -1*Math.cos(theta + Math.PI/2)*deltaV);
        yVel = (float) (yVel + -1*Math.sin(theta + Math.PI/2)*deltaV);
    }
    public List<Sprite> _update() {
        this.rx = rx + xVel;
        this.ry = ry + yVel;
        this.x = Math.round(rx);
        this.y = Math.round(ry);
        this.rotate(thetaVel);
        List<Sprite> cp = new ArrayList<Sprite>(toSpawn);
        toSpawn.clear();
        hitbox.setBounds(getCenterX() - getXBound(),getCenterY()-getYBound(),2*getXBound(), 2*getYBound());
        update();
        return cp;
    }
    public void update() {
        
    }
    public boolean inside(int mx, int my) {
        if (hitbox.contains(mx, my)) {
            if (mx - x > 0 && my - y > 0 && mx - x < hitmask.length && my - y < hitmask[0].length) {
                return hitmask[mx - x][my - y];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    public void move(int dx, int dy) {
        x = x + dx;
        y = y + dy;
    }
    public void setxy(int x, int y) {
        rx = x;
        ry = y;
    }
    public void setVelocities(float xvn, float yvn) {
        xVel = xvn;
        yVel = yvn;
    }
    public float getXVel() {
        return xVel;
    }
    public float getYVel() {
        return yVel;
    }
    public boolean deletable() {
        return (this.y < -200 || this.y > parent.getHeight() + 200 || 
                this.x < -200 || this.y > parent.getWidth() + 200);
    }
    public int compareTo(Sprite b) {
        return Integer.compare(this.layer, b.layer);
    }
    public void onclick() {
        
    }
}
