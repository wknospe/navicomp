
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class Spawner {
    int freq;
    int count;
    int x0;
    int xf;
    int y0;
    int yf;
    Random rgen = new Random();
    boolean counted = true;
    Sprite toSpawn;
    public Spawner(Sprite toSpawn, int freq, int x0, int xf, int y0, int yf) {
        this.toSpawn = toSpawn;
        this.freq = freq;
        this.x0 = x0;
        this.xf = xf;
        this.y0 = y0;
        this.yf = yf;
    }
    public List<Sprite> update() {
        List<Sprite> ret = new ArrayList();
        int x = rgen.nextInt(xf - x0) + x0;
        int y = rgen.nextInt(yf - y0) + y0;
        if (counted) {
            if (count > freq) {
                ret.add(toSpawn.copy(x, y));
                count = 0;
            }
            count ++;
        } else {
            if (rgen.nextInt(freq) == 0) {
                ret.add(toSpawn.copy(x, y));
            }
        }
        return ret;
    }
}
