/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public interface Weapon {
    public Sprite getProjectile(int x, int y, double theta);
    public boolean canFire();
    public void update();
}
