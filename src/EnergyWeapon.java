
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class EnergyWeapon implements Weapon{
    Sprite parent;
    String projectileImageFile;
    BufferedImage projectileImage;
    int damage;
    double heating = 0;
    int charge = 0;
    public EnergyWeapon(Sprite parent, String projectileImageFile, int damage) {
        this.parent = parent;
        this.projectileImageFile = projectileImageFile;
        this.damage = damage;
        try {
            File pathToFile = new File(projectileImageFile);
            projectileImage = ImageIO.read(pathToFile);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public Sprite getProjectile(int x, int y, double theta) {
        heating = heating + 15;
        charge = 0;
        EnergyProjectile proj = new EnergyProjectile(projectileImage, x, y, damage, 0, 0, parent.parent, 2, parent);
        proj.rotate(theta);
        proj.accelerate(20);
        return proj;
    }
    public boolean canFire() {
        return heating < 40 && charge >= 3;
    }
    public void update() {
        if (charge < 3) {
            charge = charge + 1;
        }
        heating = .97 * heating;
    }
}
