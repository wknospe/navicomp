/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
/**
 *
 * @author Knospe
 */
public class PlayerSprite extends Sprite implements Damageable, Shooter {
    int hp;
    boolean active = true;
    List<Weapon> weapons = new ArrayList<Weapon>();
    private boolean inertComp;
    public PlayerSprite(String imgfile, int x, int y, GraphicsWindow parent, float scaling) {
        super(imgfile, x, y, parent, scaling);
        controllable = true;
        hp = 10;
        layer = 1;
    }
    void handleControl(Set<String> keystrokes) {
        if (keystrokes.contains("LEFT")) {
            thetaVel = thetaVel - 0.1;
        }
        if (keystrokes.contains("RIGHT")) {
            thetaVel = thetaVel + 0.1;
        }
        if (keystrokes.contains("UP")) {
            yVel = yVel + (float) (-3*Math.sin(theta + Math.PI/2));
            xVel = xVel + (float) (-3*Math.cos(theta + Math.PI/2));
        }
        if (keystrokes.contains("DOWN")) {
            yVel = yVel + (float) (3*Math.sin(theta + Math.PI/2));
            xVel = xVel + (float) (3*Math.cos(theta + Math.PI/2));
        }
        if (keystrokes.contains("SPACE")) {
            if (weapons.get(0).canFire()) {
                toSpawn.add(weapons.get(0).getProjectile(getCenterX(), getCenterY(), theta));
            }
        }
        if (keystrokes.contains("I")) {
            inertComp = !inertComp;
        }
    }
    public void update() {
        if (inertComp) {
            xVel = (9f/11)*xVel;
            yVel = (9f/11)*yVel;
        }
        thetaVel = (8f/11)*thetaVel;
            thetaVel = (8f/11)*thetaVel;
        for (Weapon weapon : weapons) {
            weapon.update();
        }
    }
    @Override
    public void takeDamage(int damage) {
        hp = hp - damage;
        deleteNext = isDestroyed();
        if (deleteNext) {
            try {
                int reps = rgen.nextInt(20) + 20;
                for (int i = 0; i < reps; i++) {
                    toSpawn.add(new ExplosionParticle(getCenterX() + rgen.nextInt(20) - 10,getCenterY() + rgen.nextInt(20) - 10, parent, 4));
                }
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("File not found.");
            }
        }
    }
    @Override
    public boolean isDestroyed() {
        return (hp <= 0);
    }
    public void addWeapon(Weapon weapon) {
        weapons.add(weapon);
    }
    public List<Weapon> getWeapons() {
        return weapons;
    }
}
