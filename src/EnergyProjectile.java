
import java.awt.Image;
import java.awt.image.BufferedImage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class EnergyProjectile extends Sprite implements Damaging {
    int damage;
    Sprite parentSprite;
    public int getDamage() {
        return damage;
    }
    public EnergyProjectile(BufferedImage img, int x, int y, int damage, int xVel, int yVel, GraphicsWindow parent, float scaling, Sprite parentSprite) {
        super(img, x, y, parent, scaling);
        this.layer = -1;
        this.damage = damage;
        this.xVel = xVel;
        this.yVel = yVel;
        this.parentSprite = parentSprite;
    }
    @Override
    public void handleCollision(Sprite other) {
        if (other != parentSprite && other.solid) {
            if (other instanceof Damageable) {
                ((Damageable) other).takeDamage(damage);
            }
            deleteNext = true;
            toSpawn.addAll(ExplosionParticle.getParticles(5, getCenterX(), getCenterY(), parent));
        }
    }
}
