
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class Asteroid extends Sprite implements Damageable, Damaging {
    int hp;
    public int getDamage() {
        return 10;
    }
    public void takeDamage(int damage) {
        hp = hp - damage;
        deleteNext = isDestroyed();
        if (deleteNext) {
            if (this.scaling <= 3) {
                toSpawn.addAll(ExplosionParticle.getParticles((int) 20, getCenterX(), getCenterY(), parent));
            } else if (this.scaling <= 6) {
                toSpawn.addAll(ExplosionParticle.getParticles((int) 40, getCenterX(), getCenterY(), parent));
                Asteroid c1 = new Asteroid(file, getCenterX() + 30, getCenterY(), parent, 2);
                Asteroid c2 = new Asteroid(file, getCenterX() - 30, getCenterY(), parent, 2);
                c1.xVel = rgen.nextInt(5);
                c2.xVel = -rgen.nextInt(5);
                toSpawn.add(c1);
                toSpawn.add(c2);
            } else if (this.scaling >= 7) {
                toSpawn.addAll(ExplosionParticle.getParticles((int) 60, getCenterX(), getCenterY(), parent));
                Asteroid c1 = new Asteroid(file, getCenterX() + 30, getCenterY() + 30, parent, 2);
                Asteroid c2 = new Asteroid(file, getCenterX(), getCenterY() - 30, parent, 2);
                Asteroid c3 = new Asteroid(file, getCenterX() - 30 , getCenterY() + 30, parent, 2);
                c1.xVel = rgen.nextInt(5);
                c2.xVel = rgen.nextInt(5) - 2;
                c3.xVel = -rgen.nextInt(5);
                toSpawn.add(c1);
                toSpawn.add(c2);
                toSpawn.add(c3);
            } 
        }
    }
    @Override
    public Sprite copy(int cx, int cy) {
        Asteroid newAst = new Asteroid(file, cx, cy, parent);
        return newAst;
    }
    public void handleCollision(Sprite other) {
        if (other instanceof Damageable) {
            ((Damageable) other).takeDamage(getDamage());
        }
    }
    public boolean isDestroyed() {
        return hp <= 0;
    }
    public Asteroid(String img,int x, int y, GraphicsWindow parent) {
        super(img, x,y, parent, rgen.nextInt(6) + 2);
        this.yVel = 10 - this.scaling;
        this.xVel = rgen.nextInt(3) - 1;
        hp = (int)(this.scaling * 3);
    }
    public Asteroid(String img,int x, int y, GraphicsWindow parent, float scaling) {
        super(img, x,y, parent, scaling);
        this.yVel = 10 - this.scaling;
        hp = (int)(this.scaling * 3);
    }
}
