
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class AIShip extends Sprite implements Damageable, Shooter{
    int hp;
    boolean active = true;
    List<Weapon> weapons = new ArrayList<Weapon>();
    public AIShip(String imgfile, int x, int y, GraphicsWindow parent, float scaling) {
        super(imgfile, x, y, parent, scaling);
    }
    public void addWeapon(Weapon weapon) {
        weapons.add(weapon);
    }
    public void takeDamage(int damage) {
        hp = hp - damage;
    }
    public boolean isDestroyed() {
        return hp <= 0;
    }
    public List<Weapon> getWeapons() {
        return weapons;
    }
    public void update() {
        int px = x, py = y;
        for (Sprite sprite : parent.getSprites()) {
            if (sprite.controllable) {
                px = sprite.x;
                py = sprite.y;
                break;
            }
        }
        if      (px > x + 5) {xVel = xVel + 3;} 
        else if (px < x - 5) {xVel = xVel - 3;}
        else {
            if (xVel > 2) {
                xVel = xVel - 3;
            } else if (xVel < -2) {
                xVel = xVel + 3;
            } else {
                xVel = 0;
            }
        }
        for (Weapon weapon : weapons) {
            weapon.update();
        }
        /*if (weapons.get(0).canFire()) {
            toSpawn.add(weapons.get(0).getProjectile(getCenterX(), getCenterY() - 10));
        }*/
        xVel = (9f/11)*xVel;
        yVel = (9f/11)*yVel;
    }  
}
