
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.Timer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */

public class GraphicsWindow extends JPanel implements MouseListener {
    private List<Sprite> sprites = new LinkedList();
    private List<Sprite> toSpawn = new LinkedList();
    private List<Sprite> toDelete = new LinkedList();
    private List<Spawner> toSpawnSpawners = new LinkedList();
    private List<Spawner> toDeleteSpawners = new LinkedList();
    private List<Spawner> spawners = new ArrayList();
    private int _timeSlice = 50;  // update every 50 milliseconds
    private Timer _timer = new Timer(_timeSlice,  (e) -> this.update());
    private Set<String> keystrokes = new HashSet();
    public GraphicsWindow () {
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("pressed LEFT"), "left");
        this.getActionMap().put("left", new AbstractAction("left") { 
            public void actionPerformed(ActionEvent evt) {
                acceptKeystroke("LEFT");
            }
        });
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("pressed RIGHT"), "right");
        this.getActionMap().put("right", new AbstractAction("right") { 
            public void actionPerformed(ActionEvent evt) {
                acceptKeystroke("RIGHT");
            }
        });
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("pressed UP"), "up");
        this.getActionMap().put("up", new AbstractAction("up") { 
            public void actionPerformed(ActionEvent evt) {
                acceptKeystroke("UP");
            }
        });
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("pressed DOWN"), "down");
        this.getActionMap().put("down", new AbstractAction("down") { 
            public void actionPerformed(ActionEvent evt) {
                acceptKeystroke("DOWN");
            }
        });
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released LEFT"), "rleft");
        this.getActionMap().put("rleft", new AbstractAction("rleft") { 
            public void actionPerformed(ActionEvent evt) {
                removeKeystroke("LEFT");
            }
        });
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released RIGHT"), "rright");
        this.getActionMap().put("rright", new AbstractAction("rright") { 
            public void actionPerformed(ActionEvent evt) {
                removeKeystroke("RIGHT");
            }
        });this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released UP"), "rup");
        this.getActionMap().put("rup", new AbstractAction("rup") { 
            public void actionPerformed(ActionEvent evt) {
                removeKeystroke("UP");
            }
        });
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released DOWN"), "rdown");
        this.getActionMap().put("rdown", new AbstractAction("rdown") { 
            public void actionPerformed(ActionEvent evt) {
                removeKeystroke("DOWN");
            }
        });
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("pressed SPACE"), "space");
        this.getActionMap().put("space", new AbstractAction("space") { 
            public void actionPerformed(ActionEvent evt) {
                acceptKeystroke("SPACE");
            }
        });
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("released SPACE"), "rspace");
        this.getActionMap().put("rspace", new AbstractAction("rspace") { 
            public void actionPerformed(ActionEvent evt) {
                removeKeystroke("SPACE");
            }
        });
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("pressed I"), "pi");
        this.getActionMap().put("pi", new AbstractAction("pi") { 
            public void actionPerformed(ActionEvent evt) {
                acceptKeystroke("I");
            }
        });
    }
    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            int x = e.getX();
            int y = e.getY() - 25;
            for (Sprite sprite: sprites) {
                if (sprite.clickable && sprite.inside(x, y)) {
                    sprite.onclick();
                }
            }
        }
    }
    public void mouseExited(MouseEvent e) {
        
    }
    public void mouseEntered(MouseEvent e) {
        
    }
    public void mouseReleased(MouseEvent e) {
        
    }
    public void mouseClicked(MouseEvent e) {
        
    }
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
        g2d.setColor(new Color(0,0,0));
        g2d.fillRect(0, 0, getWidth(), getHeight());
        for (Sprite sprite : sprites) {
            sprite.draw(g2d);
        }
        g2d.dispose();
    }
    public List<Sprite> getSprites() {
        return new ArrayList(sprites);
    }
    public void asteroids() {
        toDelete.addAll(sprites);
        PlayerSprite player = new PlayerSprite("starfighter.png", getWidth()/2, getHeight() - 30, this, 2);
        Weapon laser = new EnergyWeapon(player, "laser1.png", 3);
        player.addWeapon(laser);
        toSpawn.add(player);
        Asteroid asteroid = new Asteroid("asteroid.png", getWidth()/2, 30, this);
        Spawner spawner = new Spawner((Sprite) asteroid,20, 0, getWidth(), -30, -29);
        toSpawnSpawners.add(spawner);
        repaint();
    }
    public void target() {
        toDelete.addAll(sprites);
        PlayerSprite player = new PlayerSprite("starfighter.png", getWidth()/2, getHeight() - 30, this, 2);
        Weapon laser = new EnergyWeapon(player, "laser1.png", 3);
        player.addWeapon(laser);
        toSpawn.add(player);
        Target target = new Target("asteroidButton.png", getWidth()/2, 200, this, 1);
        target.rotate(Math.PI/4);
        toSpawn.add(target);
    }
    public void aiTest() {
        toDelete.addAll(sprites);
        PlayerSprite player = new PlayerSprite("starfighter.png", getWidth()/2, getHeight() - 30, this, 2);
        Weapon laser = new EnergyWeapon(player, "laser1.png", 3);
        player.addWeapon(laser);
        toSpawn.add(player);
        AIShip ai  = new AIShip("red.png", 30, 30, this, 2);
        Weapon laser2 = new EnergyWeapon(player, "laser1.png", 3);
        ai.addWeapon(laser2);
        toSpawn.add(ai);
    }
    public void menu() {
        _timer.start();
        ButtonSprite asteroidButton = new ButtonSprite("asteroidButton.png", getWidth()/2, getHeight()/2 - 50,this,1);
        ButtonSprite targetButton = new ButtonSprite("targetButton.png", getWidth()/2, getHeight()/2, this, 1);
        ButtonSprite aiButton = new ButtonSprite("aitestButton.png", getWidth()/2, getHeight()/2 + 50, this, 1);
        asteroidButton.bind("asteroids", new Object[0], new Class[0]);
        targetButton.bind("target", new Object[0], new Class[0]);
        aiButton.bind("aiTest", new Object[0], new Class[0]);
        //aiButton.rotate(Math.PI/4);
        //aiButton.getRotatedHitmask();
        toSpawn.add(aiButton);
        toSpawn.add(asteroidButton);
        toSpawn.add(targetButton);
    }
    public void acceptKeystroke(String keystroke) {
        this.keystrokes.add(keystroke);
    }
    public void removeKeystroke(String keystroke) {
        this.keystrokes.remove(keystroke);
    }
    public void handleKeystrokes() {
        for (Sprite sprite: sprites) {
            if (sprite.controllable) {
                PlayerSprite pSprite = (PlayerSprite) sprite;
                if (pSprite.active) {
                    pSprite.handleControl(keystrokes);
                }
            }
        }
        keystrokes.remove("I");
        //keystrokes.clear();
    }
    public void update() {
        handleKeystrokes();
        for (Sprite sprite: sprites) {
            if (sprite.solid) {
                for (Sprite sprite2: sprites) {
                    if (sprite2.solid && sprite != sprite2 && sprite.checkCollision(sprite2)) {
                        sprite.handleCollision(sprite2);
                    }
                }
            }
        }
        for (Sprite sprite: sprites) {
            if (sprite.deletable() || sprite.deleteNext) {
                toDelete.add(sprite);
            }
            List<Sprite> nSpr = sprite._update();
            if (nSpr != null){
                toSpawn.addAll(nSpr);
            }
        }
        for (Spawner spawner: spawners) {
            toSpawn.addAll(spawner.update());
        }
        if (toSpawnSpawners.size() > 0) {
            spawners.addAll(toSpawnSpawners);
            toSpawnSpawners.clear();
        }
        if (toDeleteSpawners.size() > 0) {
            spawners.removeAll(toDeleteSpawners);
            toDeleteSpawners.clear();
        }
        boolean spriteUpdate = false;
        if (toSpawn.size() > 0) {
            sprites.addAll(toSpawn);
            spriteUpdate = true;
            toSpawn.clear();
        }
        if (toDelete.size() > 0) {
            sprites.removeAll(toDelete);
            spriteUpdate = true;
            toDelete.clear();
        }
        if (spriteUpdate) {
            Collections.sort(sprites);
        }
        repaint();
    }
}
