import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class ButtonSprite extends Sprite {
    Method func;
    Object[] params;
    ButtonSprite(String img, int x, int y, GraphicsWindow parent, float scaling) {
        super(img, x, y, parent, scaling);
        solid = false;
        clickable = true;
        //this.params = params;
    }
    public void bind(String action, Object[] params, Class[] parameterTypes) {
        try {
            this.func = GraphicsWindow.class.getMethod(action, parameterTypes);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error!");
        }
    }
    @Override
    public void onclick() {
        try {
            if (func != null) {
                func.invoke(parent, params);
            }
        } catch (IllegalAccessException ex) {
            throw new RuntimeException("There was an error.");
        } catch (IllegalArgumentException ex) {
            throw new RuntimeException("There was an error.");
        } catch (InvocationTargetException ex) {
            throw new RuntimeException("There was an error.");
        }
    }
}
