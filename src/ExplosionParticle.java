
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class ExplosionParticle extends Sprite{
    static List<String> images = new ArrayList<String>(Arrays.asList("rpart.png", "opart.png", "ypart.png"));
    static List<ExplosionParticle> getParticles(int reps, int x,int y, GraphicsWindow parent) {
        List<ExplosionParticle> spawn = new ArrayList();
        try {
            for (int i = 0; i < reps; i++) {
                spawn.add(new ExplosionParticle(x + rgen.nextInt(reps) - reps/2,y + rgen.nextInt(reps) - reps/2, parent, 4));
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("File not found.");
        }
        return spawn;
    }
    public ExplosionParticle(int x, int y, GraphicsWindow parent, float scaling) throws IOException {
        super(images.get(rgen.nextInt(images.size())), x, y, parent, scaling);
        layer = 1;
        solid = false;
    }
    @Override
    public void update() {
        x = x + rgen.nextInt(7) - 3;
        y = y + rgen.nextInt(7) - 3;
        deleteNext = (rgen.nextInt(10) == 1);
    }
}
