
import java.awt.Image;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Knospe
 */
public class Target extends Sprite implements Damageable{
    public Target(String imgstring, int x, int y, GraphicsWindow parent, float scaling) {
        super(imgstring, x, y, parent, scaling);
    }
    int hp = 1;
    public boolean isDestroyed() {
        return hp <= 0;
    }
    public void takeDamage(int damage) {
        hp = hp - damage;
        deleteNext = isDestroyed();
        if (deleteNext) {
            toSpawn.addAll(ExplosionParticle.getParticles(30, getCenterX(), getCenterY(), parent));
        }
    }
}
